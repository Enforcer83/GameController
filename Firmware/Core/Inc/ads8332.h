//***************************************************************************************
//
// ADS8332 - 8 Port Multiplex 500ksps 24-bit ADC Library Header
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _ADC_ADS8332_H_
#define _ADC_ADS8332_H_

ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"  // Change this line for the processor family in use.
#include <stdbool.h>

#define ADS8332_REG_SEL_CH_0	0x0000
#define ADS8332_REG_SEL_CH_1	0x1000
#define ADS8332_REG_SEL_CH_2	0x2000
#define ADS8332_REG_SEL_CH_3	0x3000
#define ADS8332_REG_SEL_CH_4	0x4000
#define ADS8332_REG_SEL_CH_5	0x5000
#define ADS8332_REG_SEL_CH_6	0x6000
#define ADS8332_REG_SEL_CH_7	0x7000
#define ADS8332_REG_WAKE		0xB000
#define ADS8332_REG_READ_CFR	0xC000
#define ADS8332_REG_READ_DATA	0xD000
#define ADS8332_REG_WRITE_CFR	0xE000
#define ADS8332_REG_DEFAULT		0xF000

#define ADS8332_CH_SEL_MAN		0x0000
#define ADS8332_CH_SEL_AUTO		0x0800

#define ADS8332_MAX_CHANNELS	8

typedef struct
{

	// SPI handlers
	SPI_HandleTypeDef *_spiHandle;
	GPIO_TypeDef *_csPort;
	uint16_t _csPin;
	bool _usingHW_CS;

	// Status IO Handlers
	GPIO_TypeDef *_intEOC_Port;
	GPIO_TypeDef *_rstPort;
	GPIO_TypeDef *_convstPort;
	uint16_t _intEOC_Pin;
	uint16_t _rstPin;
	uint16_t _convstPin;

	// Configuration
	volatile uint16_t chSelectMode;
	volatile uint16_t cClkSource;
	volatile uint16_t triggerSelect;
	volatile uint16_t sampleRateSel;
	volatile uint16_t polIntEOC;
	volatile uint16_t funcIntEOC;
	volatile uint16_t daisyChain;
	volatile uint16_t autoNAP;
	volatile uint16_t enNAP;
	volatile uint16_t enDeepPwrDn;
	volatile uint16_t enableTAG;
	volatile uint16_t swRst;

	volatile uint16_t config;

	uint16_t _channels;

	// Data Buffers
	volatile uint16_t txBuf[ADS8332_MAX_CHANNELS];
	volatile uint16_t rxBuf[ADS8332_MAX_CHANNELS];

} ADS8332;

extern void Error_Handler();

void ADS8332_Init(ADS8332 *adc, );

void ADS8332_CH_Sel_Man(ADS8332 *adc);
void ADS8332_CH_Sel_Auto(ADS8332 *adc);
void ADS8332_Conv_Clk_Internal(ADS8332 *adc);
void ADS8332_Conv_Clk_SCLK(ADS8332 *adc);
void ADS8332_Man_Trigger(ADS8332 *adc);
void ADS8332_Auto_Trigger(ADS8332 *adc);
void ADS8332_SampleRate_500ksps(ADS8332 *adc);
void ADS8332_SampleRate_250ksps(ADS8332 *adc);
void ADS8332_IntEOC_Low(ADS8332 *adc);
void ADS8332_IntEOC_High(ADS8332 *adc);
void ADS8332_EOC(ADS8332 *adc);
void ADS8332_Int(ADS8332 *adc);
void ADS8332_Int_EOC(ADS8332 *adc);
void ADS8332_DaisyChain(ADS8332 *adc);
void ADS8332_AutoNAP_Enable(ADS8332 *adc);
void ADS8332_AutoNAP_Disable(ADS8332 *adc);
void ADS8332_NAP_PwrDn_Enable(ADS8332 *adc);
void ADS8332_NAP_PwrDn_Disable(ADS8332 *adc);
void ADS8332_DeepPwrDn_Disable(ADS8332 *adc);
void ADS8332_DeepPwrDn_Enable(ADS8332 *adc);
void ADS8332_TAG_Enable(ADS8332 *adc);
void ADS8332_TAG_Disable(ADS8332 *adc);
void ADS8332_Norm(ADS8332 *adc);
void ADS8332_SW_Rst(ADS8332 *adc);

void ADS8332_Auto_Sample(ADS8332 *adc);
void ADS8332_Man_Sample(ADS8332 *adc, uint16_t channel);

#ifdef __cplusplus
}
#endif

#endif /* _ADC_ADS8332_H_ */
