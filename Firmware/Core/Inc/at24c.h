//***************************************************************************************
//
// AT24Cxxx - I2C-Compatible Serial EEPROM Library Header
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _MEM_AT24C_H_
#define _MEM_AT24C_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"  // Change this line for the processor family in use.
#include <stdbool.h>

typedef enum
{

	Bit16 = 16, Bit32 = 32, Bit64 = 64, Bit128 = 128, Bit256 = 256, Bit512 = 512,
	Kilobit1 = 1024, Kilobit2 = 2048, Kilobit4 = 4096, Kilobit8 = 8192,
	Kilobit16 = 16384, Kilobit32 = 32768, Kilobit64 = 65536, Kilobit128 = 131072,
	Kilobit256 = 262144, Kilobit512 = 524288, Megabit1 = 1048576, Megabit2 = 2097152

} Mem_SizeTypedef;

typedef struct
{

	// I2C handlers
	I2C_HandleTypeDef *_i2cHandle;
	uint8_t _i2cAddr;

	// Interrupt handlers
	GPIO_TypeDef *_wpPort;
	uint16_t _wpPin;
	bool writeProtected;

	// EEPROM Information
	Mem_SizeTypedef _memSize;

} AT24C;

extern void Error_Handler();

void AT24C_Mem_Init(AT24C *mem, I2C_HandleTypeDef *i2cHandle, uint8_t addr, Mem_SizeTypedef memSize);
void AT24C_Mem_WP_Init(AT24C *mem, I2C_HandleTypeDef *i2cHandle, uint8_t addr, Mem_SizeTypedef memSize, GPIO_TypeDef *wpPort, uint16_t wpPin);

HAL_StatusTypeDef AT24C_MemRead(AT24C *mem, uint32_t memAddr, uint8_t* data, uint32_t length);
HAL_StatusTypeDef AT24C_MemWrite(AT24C *mem, uint32_t memAddr, uint8_t* data, uint32_t length);

HAL_StatusTypeDef AT24C_SetWP(AT24C* mem);
HAL_StatusTypeDef AT24C_ClearWP(AT24C* mem);
HAL_StatusTypeDef AT24C_ToggleWP(AT24C* mem);

#ifdef __cplusplus
}
#endif

#endif /* _MEM_AT24C_H_ */
