//***************************************************************************************
//
// TCA9555 - 16-Bit I2C I/O Expander Library Header
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _IOEXP_TCA9555_H_
#define _IOEXP_TCA9555_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"  // Change this line for the processor family in use.

// Register Address Defines
#define	TCA9555_REG_INPUT0	0x00
#define	TCA9555_REG_INPUT1	0x01
#define	TCA9555_REG_OUTPUT0	0x02
#define	TCA9555_REG_OUTPUT1	0x03
#define	TCA9555_REG_POL0	0x04
#define	TCA9555_REG_POL1	0x05
#define	TCA9555_REG_CONFIG0	0x06
#define	TCA9555_REG_CONFIG1	0x07

typedef struct
{

	// I2C handlers
	I2C_HandleTypeDef *_i2cHandle;
	uint8_t _i2cAddr;

	// Interrupt handlers
	GPIO_TypeDef *_intPort;
	uint16_t _intPin;

	uint8_t TCA9555_REG_DATA[8];

} IOExp_TCA9555;

typedef enum
{

	TCA9555_PortA = 0,
	TCA9555_PortB

} TCA9555_Port;

typedef enum
{

	TCA9555_PIN_RESET = 0,
	TCA9555_PIN_SET

} TCA9555_PinState;

// Initialization and configuration functions
void tca9555_Init(IOExp_TCA9555 *ioexp, I2C_HandleTypeDef *i2cHandle, uint8_t addr, GPIO_TypeDef *intPort, uint16_t intPin);
void tca9555_ConfigPort(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t config);
void tca9555_PolarityPort(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pol);

// Single Pin Functions
TCA9555_PinState tca9555_GPIO_ReadPin(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pin);
void tca9555_GPIO_WritePin(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pin, TCA9555_PinState state);
void tca9555_GPIO_TogglePin(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pin);

// Multi-Pin Functions
uint16_t tca9555_GPIO_ReadPort(IOExp_TCA9555 *ioexp, TCA9555_Port port);
void tca9555_GPIO_WritePort(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t data);
void tca9555_GPIO_TogglePort(IOExp_TCA9555 *ioexp, TCA9555_Port port);

// Low Level Functions
HAL_StatusTypeDef tca9555_ReadReg(IOExp_TCA9555 *ioexp, uint8_t regAddr, uint8_t *data, uint8_t length);
HAL_StatusTypeDef tca9555_WriteReg(IOExp_TCA9555 *ioexp, uint8_t regAddr, uint8_t data);

#ifdef __cplusplus
}
#endif

#endif /* _IOEXP_TCA9555_H_*/
