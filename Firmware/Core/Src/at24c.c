//***************************************************************************************
//
// AT24Cxxx - I2C-Compatible Serial EEPROM Library Source
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

//***************************************************************************************
//
// HAL_StatusTypeDef
//
//  typedef enum
//  {
//      HAL_OK       = 0x00U,
//      HAL_ERROR    = 0x01U,
//      HAL_BUSY     = 0x02U,
//      HAL_TIMEOUT  = 0x03U
//  } HAL_StatusTypeDef;
//
//***************************************************************************************

#include "at24c.h"

//
// Initialize all variables to communicate with and identify the memory; no control of
// the Write Protection (WP) pin.
//
void AT24C_Mem_Init(AT24C* mem, I2C_HandleTypeDef *i2cHandle, uint8_t addr, Mem_SizeTypedef memSize)
{

	// Store the I2C handle
    mem->_i2cHandle = i2cHandle;
    
    // Determine the format of the addr value and properly set the device address
    if ((addr & 0xF0) == 0xA0)
    {
        
		// addr value is already properly formated
        mem->_i2cAddr = addr & 0xFC;
        
    }
    
    else if ((addr & 0xF8) == 0x28) 
    {
    
        // addr value is the true address before bit shift
		mem->_i2cAddr = addr << 2;
    
    }
    
    else
    {
		// addr value reflects last two bits of address
        mem->_i2cAddr = (addr << 2) | 0xA0 ;
    
    }
    
    // Properly format and set the memory size
    if ((memSize % 8) == 0)
    {
    
        mem->_memSize = memSize;
        
    }
    
    else
    {
		// Eventually this conditional will properly format the memory size for any size
		// memory, however, for now if the value is not fully divisible by 8 then there
		// is an error that needs to be inspected
        Error_Handler();
    
    }

}

//
// Initialize all variables to communicate with and identify the memory; control of
// the Write Protection (WP) pin provided.
//
void AT24C_Mem_WP_Init(AT24C* mem, I2C_HandleTypeDef *i2cHandle, uint8_t addr, Mem_SizeTypedef memSize, GPIO_TypeDef* wpPort, uint16_t wpPin)
{

    // Store the I2C handle
    mem->_i2cHandle = i2cHandle;
    
    // Determine the format of the addr value and properly set the device address
    if ((addr & 0xF0) == 0xA0)
    {
        
		// addr value is already properly formated
        mem->_i2cAddr = addr & 0xFC;
        
    }
    
    else if ((addr & 0xF8) == 0x28) 
    {
    
        // addr value is the true address before bit shift
		mem->_i2cAddr = addr << 2;
    
    }
    
    else
    {
		// addr value reflects last two bits of address
        mem->_i2cAddr = (addr << 2) | 0xA0 ;
    
    }
    
    // Stor the handles for the write protection GPIO port and pin.
    mem->_wpPort = wpPort;
    mem->_wpPin = wpPin;
    
	// Ensure the memory is not write protected.
    AT24C_ClearWP(mem);
    
    // Properly format and set the memory size
    if ((memSize % 8) == 0)
    {
    
        mem->_memSize = memSize;
        
    }
    
    else
    {
    
        // Eventually this conditional will properly format the memory size for any size
		// memory, however, for now if the value is not fully divisible by 8 then there
		// is an error that needs to be inspected
        Error_Handler();
    
    }
    
}

//
// Read data from memory.  Checks that the ammount of data being read is not more than
// what is available.
//
HAL_StatusTypeDef AT24C_Mem_Read(AT24C* mem, uint32_t memAddr, uint8_t* data, uint32_t length)
{

	uint8_t devAddrMod = 0;

    if ((length > 256) && ((memAddr + length) < mem->_memSize))
    {
        
    	uint32_t currentMemAddr = memAddr;
        uint32_t current = length;
        uint32_t loops = 0;
        HAL_StatusTypeDef currentStatus = HAL_OK;
        HAL_StatusTypeDef prevStatus = HAL_OK;
        
        while (current > 256)
        {
            
        	devAddrMod = ((currentMemAddr & 0x00010000) >> 15) | mem->_i2cAddr;

            currentStatus = HAL_I2C_Mem_Read(mem->_i2cHandle, devAddrMod, (uint16_t)currentMemAddr, I2C_MEMADD_SIZE_16BIT, data[loops * 256], 256, HAL_MAX_DELAY);

            if (currentStatus > prevStatus)
            {
                
                prevStatus = currentStatus;
                
            }
            
            currentMemAddr += 256;
            loops++;
            current -= 256;
            
        }
        
        devAddrMod = ((currentMemAddr & 0x00010000) >> 15) | mem->_i2cAddr;

        currentStatus = HAL_I2C_Mem_Read(mem->_i2cHandle, devAddrMod, (uint16_t)currentMemAddr, I2C_MEMADD_SIZE_16BIT, data[loops * 256], current, HAL_MAX_DELAY);
            
        if (currentStatus > prevStatus)
        {
                
            prevStatus = currentStatus;
                
        }
        
        return prevStatus;
        
    }
    
    else if ((memAddr + length) < mem->_memSize)
    {
        
    	devAddrMod = ((memAddr & 0x00010000) >> 15) | mem->_i2cAddr;

        return HAL_I2C_Mem_Read(mem->_i2cHandle, devAddrMod, (uint16_t)memAddr, I2C_MEMADD_SIZE_16BIT, data, length, HAL_MAX_DELAY);
        
    }
    
    else
    {
        
        return HAL_ERROR;
        
    }

}

//
// Write data to memory.  Checks that the ammount of data being written is not more than
// what is available.
//
HAL_StatusTypeDef AT24C_Mem_Write(AT24C* mem, uint32_t memAddr, uint8_t* data, uint32_t length)
{

	uint8_t devAddrMod = 0;

    if ((length > 256) && ((memAddr + length) < mem->_memSize))
    {
        
    	uint32_t currentMemAddr = memAddr;
        uint32_t current = length;
        uint32_t loops = 0;
        HAL_StatusTypeDef currentStatus = HAL_OK;
        HAL_StatusTypeDef prevStatus = HAL_OK;
        
        while (current > 256)
        {
            
        	devAddrMod = ((currentMemAddr & 0x00010000) >> 15) | mem->_i2cAddr;

            currentStatus = HAL_I2C_Mem_Write(mem->_i2cHandle, devAddrMod, (uint16_t)currentMemAddr, I2C_MEMADD_SIZE_16BIT, data[loops * 256], 256, HAL_MAX_DELAY);

            if (currentStatus > prevStatus)
            {
                
                prevStatus = currentStatus;
                
            }
            
            currentMemAddr += 256;
            loops++;
            current -= 256;
            
        }
        
        devAddrMod = ((currentMemAddr & 0x00010000) >> 15) | mem->_i2cAddr;

        currentStatus = HAL_I2C_Mem_Write(mem->_i2cHandle, devAddrMod, (uint16_t)currentMemAddr, I2C_MEMADD_SIZE_16BIT, data[loops * 256], current, HAL_MAX_DELAY);
            
        if (currentStatus > prevStatus)
        {
                
            prevStatus = currentStatus;
                
        }
        
        return prevStatus;
        
    }
    
    else if ((memAddr + length) < mem->_memSize)
    {
        
    	devAddrMod = ((memAddr & 0x00010000) >> 15) | mem->_i2cAddr;

        return HAL_I2C_Mem_Write(mem->_i2cHandle, devAddrMod, (uint16_t)memAddr, I2C_MEMADD_SIZE_16BIT, data, length, HAL_MAX_DELAY);
        
    }
    
    else
    {
        
        return HAL_ERROR;
        
    }

}

//
// Write Protect the EEPROM
//
HAL_StatusTypeDef AT24C_SetWP(AT24C* mem)
{
    
    mem->writeProtected = true;
    
    HAL_GPIO_WritePin(mem->_wpPort, mem->_wpPin, GPIO_PIN_SET);

    return HAL_OK;
    
}

//
// Allow EEPROM to be Written to
//
HAL_StatusTypeDef AT24C_ClearWP(AT24C* mem)
{
    
    mem->writeProtected = false;
    
    HAL_GPIO_WritePin(mem->_wpPort, mem->_wpPin, GPIO_PIN_RESET);

    return HAL_OK;
    
}

// 
// Toggle the Write Protection Pin
//
HAL_StatusTypeDef AT24C_ToggleWP(AT24C* mem)
{
    
    mem->writeProtected ^= true;
    
    HAL_GPIO_TogglePin(mem->_wpPort, mem->_wpPin);

    return HAL_OK;
    
}
