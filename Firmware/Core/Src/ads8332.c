//***************************************************************************************
//
// ADS8332 - 8 Port Multiplex 500ksps 24-bit ADC Library Source
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "ads8332.h"

void ADS8332_Init(ADS8332 *adc, SPI_HandleTypeDef *spiHandle, GPIO_TypeDef* csPort, uint16_t csPin, bool usingHW_CS, uint16_t channels)
{

    adc->_spiHandle = spiHandle;
    adc->_csPort = csPort;
    adc->_csPin = csPin;
    adc->_channels = channels;
    adc->_usingHW_CS = usingHW_CS;

    adc->config = 0;

    for (uint8_t i = 0; i < ADS8332_MAX_CHANNELS; i++)
    {

        adc->txBuf[i] = 0;
        adc->rxBuf[i] = 0;

    }

}

void ADS8332_CH_Sel_Man(ADS8332 *adc)
{


    
}

void ADS8332_CH_Sel_Auto(ADS8332 *adc)
{



}

void ADS8332_Conv_Clk_Internal(ADS8332 *adc)
{



}

void ADS8332_Conv_Clk_SCLK(ADS8332 *adc)
{



}

void ADS8332_Man_Trigger(ADS8332 *adc)
{



}

void ADS8332_Auto_Trigger(ADS8332 *adc)
{



}

void ADS8332_SampleRate_500ksps(ADS8332 *adc)
{



}

void ADS8332_SampleRate_250ksps(ADS8332 *adc)
{



}

void ADS8332_IntEOC_Low(ADS8332 *adc)
{



}

void ADS8332_IntEOC_High(ADS8332 *adc)
{



}

void ADS8332_EOC(ADS8332 *adc)
{



}

void ADS8332_Int(ADS8332 *adc)
{



}

void ADS8332_Int_EOC(ADS8332 *adc)
{



}

void ADS8332_DaisyChain(ADS8332 *adc)
{



}

void ADS8332_AutoNAP_Enable(ADS8332 *adc)
{



}

void ADS8332_AutoNAP_Disable(ADS8332 *adc)
{



}

void ADS8332_NAP_PwrDn_Enable(ADS8332 *adc)
{



}

void ADS8332_NAP_PwrDn_Disable(ADS8332 *adc)
{



}

void ADS8332_DeepPwrDn_Disable(ADS8332 *adc)
{



}

void ADS8332_DeepPwrDn_Enable(ADS8332 *adc)
{



}

void ADS8332_TAG_Enable(ADS8332 *adc)
{



}

void ADS8332_TAG_Disable(ADS8332 *adc)
{



}

void ADS8332_Norm(ADS8332 *adc)
{



}

void ADS8332_SW_Rst(ADS8332 *adc)
{



}

void ADS8332_Auto_Sample(ADS8332 *adc)
{



}

void ADS8332_Man_Sample(ADS8332 *adc, uint16_t channel)
{



}
