//***************************************************************************************
//
// TCA9555 - 16-Bit I2C I/O Expander Library Source
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "tca9555.h"

// Initialization function(s)
void tca9555_Init(IOExp_TCA9555 *ioexp, I2C_HandleTypeDef *i2cHandle, uint8_t addr, GPIO_TypeDef *intPort, uint16_t intPin)
{

	ioexp->_i2cHandle = i2cHandle;
	ioexp->_intPort = intPort;
	ioexp->_intPin = intPin;

	if ((addr & 0xf0) == 0x20) {

		ioexp->_i2cAddr = (addr << 1);

	}

	else if ((addr & 0xf0) == 0x40) {

		ioexp->_i2cAddr = addr;

	}

	else {

		ioexp->_i2cAddr = 0x40 | ((addr & 0x0F) << 1);

	}

	for (uint8_t i = TCA9555_REG_INPUT0; i < TCA9555_REG_CONFIG1; i++) {

		if ((i == TCA9555_REG_OUTPUT0) || (i == TCA9555_REG_OUTPUT1)) {

			ioexp->TCA9555_REG_DATA[i] = 0xff;

		}

		else {

			ioexp->TCA9555_REG_DATA[i] = 0x00;

		}

	}

}

void tca9555_ConfigPort(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t config)
{

	uint8_t regAddr;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_CONFIG0;

	}

	else {

		regAddr = TCA9555_REG_CONFIG1;

	}

	ioexp->TCA9555_REG_DATA[regAddr] = config;

	tca9555_WriteReg(ioexp, regAddr, ioexp->TCA9555_REG_DATA[regAddr]);

}

void tca9555_PolarityPort(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pol)
{

	uint8_t regAddr;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_POL0;

	}

	else {

		regAddr = TCA9555_REG_POL1;

	}

	ioexp->TCA9555_REG_DATA[regAddr] = pol;

	tca9555_WriteReg(ioexp, regAddr, ioexp->TCA9555_REG_DATA[regAddr]);

}

// Single Pin Functions
TCA9555_PinState tca9555_GPIO_ReadPin(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pin)
{

	uint8_t regAddr;
	uint8_t regData;
	uint8_t mask = 1 << pin;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_OUTPUT0;

	}

	else {

		regAddr = TCA9555_REG_OUTPUT1;

	}

	tca9555_ReadReg(ioexp, regAddr, &regData, 1);

	if ((regData & mask) > 0) {

		return TCA9555_PIN_SET;

	}

	else {

		return TCA9555_PIN_RESET;

	}

}

void tca9555_GPIO_WritePin(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pin, TCA9555_PinState state)
{

	uint8_t regAddr;
	uint8_t mask = 1 << pin;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_OUTPUT0;

	}

	else {

		regAddr = TCA9555_REG_OUTPUT1;

	}

	ioexp->TCA9555_REG_DATA[regAddr] &= ~mask;

	if (state) {

		ioexp->TCA9555_REG_DATA[regAddr] |= mask;

	}

	tca9555_WriteReg(ioexp, regAddr, ioexp->TCA9555_REG_DATA[regAddr]);

}

void tca9555_GPIO_TogglePin(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t pin)
{

	uint8_t regAddr;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_OUTPUT0;

	}

	else {

		regAddr = TCA9555_REG_OUTPUT1;

	}

	ioexp->TCA9555_REG_DATA[regAddr] = ioexp->TCA9555_REG_DATA[regAddr] ^ pin;

	tca9555_WriteReg(ioexp, regAddr, ioexp->TCA9555_REG_DATA[regAddr]);

}

// Multi-Pin Functions
uint16_t tca9555_GPIO_ReadPort(IOExp_TCA9555 *ioexp, TCA9555_Port port)
{

	uint8_t regAddr;
	uint8_t regData;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_OUTPUT0;

	}

	else {

		regAddr = TCA9555_REG_OUTPUT1;

	}

	tca9555_ReadReg(ioexp, regAddr, &regData, 1);

	return regData;

}

void tca9555_GPIO_WritePort(IOExp_TCA9555 *ioexp, TCA9555_Port port, uint8_t data)
{

	uint8_t regAddr;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_OUTPUT0;

	}

	else {

		regAddr = TCA9555_REG_OUTPUT1;

	}

	ioexp->TCA9555_REG_DATA[regAddr] = data;

	tca9555_WriteReg(ioexp, regAddr, ioexp->TCA9555_REG_DATA[regAddr]);

}

void tca9555_GPIO_TogglePort(IOExp_TCA9555 *ioexp, TCA9555_Port port)
{

	uint8_t regAddr;

	if (port == TCA9555_PortA) {

		regAddr = TCA9555_REG_OUTPUT0;

	}

	else {

		regAddr = TCA9555_REG_OUTPUT1;

	}

	ioexp->TCA9555_REG_DATA[regAddr] ^= ioexp->TCA9555_REG_DATA[regAddr];

	tca9555_WriteReg(ioexp, regAddr, ioexp->TCA9555_REG_DATA[regAddr]);

}

// Low Level Functions
HAL_StatusTypeDef tca9555_ReadReg(IOExp_TCA9555 *ioexp, uint8_t regAddr, uint8_t *data, uint8_t length)
{

	HAL_I2C_Master_Transmit(ioexp->_i2cHandle, ioexp->_i2cAddr, &regAddr, 1, 100);

	return HAL_I2C_Master_Receive(ioexp->_i2cHandle, ioexp->_i2cAddr, data, length, 100);

}

HAL_StatusTypeDef tca9555_WriteReg(IOExp_TCA9555 *ioexp, uint8_t regAddr, uint8_t data)
{

	uint8_t txBuf[2];

	txBuf[0] = regAddr;
	txBuf[1] = data;

	return HAL_I2C_Master_Transmit(ioexp->_i2cHandle, ioexp->_i2cAddr, txBuf, 2, 100);

}
